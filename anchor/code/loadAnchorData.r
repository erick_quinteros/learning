##########################################################
#
#
# aktana- anchor location estimates Aktana Learning Engines.
#
# description: read data for estimate rep likelihood of being in a location
#
# created by : marc.cohen@aktana.com
# updated by : shirley.xu@aktana.com
#
# created on : 2016-11-01
# updated on : 2018-07-31
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(Learning.DataAccessLayer)
library(data.table)
library(futile.logger)

loadAnchorData.Interaction <- function(startDate, endDate) {
  
  loadStartDate <- format(startDate, "%Y-%m-%d")
  loadEndDate <- format(endDate, "%Y-%m-%d")
  
  # get the interactionTypeId for interactionTypeName contains "visit"
  interactionTypeIds <- dataAccessLayer.anchor.get.interactionTypeIds()
  
  interactions <- data.table(dataAccessLayer.anchor.get.interactions(loadStartDate, loadEndDate, interactionTypeIds))
  interactions$date <- as.Date(interactions$startDateLocal)
  interactions$startDateLocal <- NULL

  flog.info('finish read Interaction Data, dim=(%s)', paste(dim(interactions),collapse=","))
  return(interactions)
}

loadAnchorData.validReps <- function() {
  validReps <- dataAccessLayer.anchor.get.validReps()
  flog.info('finish read validReps Data, dim=(%s)', paste(dim(validReps),collapse=","))
  return(validReps)
}

loadAnchorData.repAccountAssignment <- function(predictRundate, validReps) {
  loadPredictRundate <- format(predictRundate, "%Y-%m-%d")
  # get valid replist
  validRepIds <- validReps[["repId"]]
  # load repAccountAssignment
  repAccountAssignments <- data.table(dataAccessLayer.anchor.get.repAccountAssignments(loadPredictRundate, validRepIds))
  flog.info('finish read repAccountAssignments Data, dim=(%s)', paste(dim(repAccountAssignments),collapse=","))
  
  return(repAccountAssignments)
}

loadAnchorData.filterInteraction <- function(interactions, repAccountAssignments) {
  # filter interactions using repAccountAssignment
  accounts_unique <- unique(interactions$accountId)
  reps_unique <- unique(interactions$repId)
  # filter on account,rep pair for active repAccountAssignemnts
  repAccountAssignments <- repAccountAssignments[repId %in% reps_unique & accountId %in% accounts_unique,]
  # filter interactions using join
  interactions <- merge(interactions, repAccountAssignments, by=c('repId', 'accountId'), all=FALSE)
  
  flog.info('finish filter Interaction Data using repAccountAssignments, dim=(%s)', paste(dim(interactions),collapse=","))
  return(interactions)
}

loadAnchorData.repTeamRep <- function() {
  repTeamRep <- data.table(dataAccessLayer.anchor.get.repTeamRep())
  flog.info('finish loading RepTeamRep Data, dim=(%s)', paste(dim(repTeamRep),collapse=","))
  return(repTeamRep)
}

loadAnchorData.accountDateLocationScores <- function() {
  accountDateLocationScores <- data.table(dataAccessLayer.anchor.get.accountDateLocationScores())
  flog.info('finish loading accountDateLocationScores Data, dim=(%s)', paste(dim(accountDateLocationScores),collapse=","))
  return(accountDateLocationScores)
}

loadAnchorData.repCalendarAdherence <- function() {
  repCalendarAdherence <- data.table(dataAccessLayer.anchor.get.repCalendarAdherence())
  flog.info('finish loading repCalendarAdherence Data, dim=(%s)', paste(dim(repCalendarAdherence),collapse=","))
  return(repCalendarAdherence)
}

loadAnchorData.repAccountCalendarAdherence <- function() {
  repAccountCalendarAdherence <- data.table(dataAccessLayer.anchor.get.repAccountCalendarAdherence())
  flog.info('finish loading repAccountCalendarAdherence Data, dim=(%s)', paste(dim(repAccountCalendarAdherence),collapse=","))
  return(repAccountCalendarAdherence)
}

loadAnchorData.CallHistory <- function(startDate, predictRunDate) {
  CallHistory <- data.table(dataAccessLayer.anchor.get.CallHistory(startDate, predictRunDate))
  flog.info('finish loading STG_Call2_H Data, dim=(%s)', paste(dim(CallHistory),collapse=","))
  return(CallHistory)
}

loadAnchorData.newRepList <- function(interactions, repAccountAssignments) {
  # construct result holder
  newRepList <- data.table()
  # get the new reps as the reps not in teractions but in repAccountAssignments
  unqIntsReps <- unique(interactions$repId)
  unqRasReps <- unique(repAccountAssignments$repId)
  newReps <- unqRasReps[!unqRasReps %in% unqIntsReps]
  if (length(newReps)>0) {
    newRepList <- data.table(repId=newReps, source="territory_noInts", predictAheadDayList=as.character(NA), stringsAsFactors=FALSE)
  }
  flog.info("finishing filtering new rep by comparing repAccountAssignments and interactions, length=%s", nrow(newRepList))
  return(newRepList)
}

loadAnchorData.accounts <- function() {
  accounts <- dataAccessLayer.anchor.get.accounts()
  flog.info('finish read account Data, dim=(%s)', paste(dim(accounts),collapse=","))
  return(accounts)
}


##########################################
##  Main loadAnchorData function
##########################################
loadAnchorData <- function(predictRundate, startDate, predictAheadDayList, calculate_adl, calendarAdherenceOn, calculate_calendarAdherence) {
  
  flog.info("Start LoadAnchorData")
  loadedDataList <- list()
  
  # DB set up
  dataAccessLayer.common.setNames.dse()
  dataAccessLayer.common.setNames.stage()
  dataAccessLayer.common.setNames.archive()
  
  # load data from DB
  interactions <- loadAnchorData.Interaction(startDate, max(predictAheadDayList))
  validReps <- loadAnchorData.validReps()
  repAccountAssignments <- loadAnchorData.repAccountAssignment(predictRundate, validReps)
  repTeamRep <- loadAnchorData.repTeamRep()
  
  # load and process data needed for calculation of AccountDateLocation
  if (calculate_adl) {
    INTERACTIONS_ACCOUNT <- interactions[date<predictRundate & isCompleted==1, -c("repId", "isCompleted", "date")]
    accountDateLocationScores <- NULL
    flog.info("calculate_adl=True, processing interactions for recalculate AccountDateLocation, dim=(interactions)=(%s)", paste(dim(INTERACTIONS_ACCOUNT),collapse=","))
  } else {
    INTERACTIONS_ACCOUNT <- NULL
    accountDateLocationScores <- loadAnchorData.accountDateLocationScores()
  }
  
  # filter interaction data using repAccountAssignment
  interactions <- interactions[,-c("timeZoneId", "startDateTime")] # only used for accountDateLocation input
  interactions <- loadAnchorData.filterInteraction(interactions, repAccountAssignments)
  
  # process interaction to get date neeed for calculateAnchor
  # subset interactions to get planned visits
  future <- interactions[date>=predictRundate & isCompleted==0]
  flog.info("finish filter out planned future events, dim=(%s)",paste(dim(future),collapse=","))
  # subset interactions to get historical interactions
  INTERACTIONS_REP <- interactions[date<predictRundate & isCompleted==1, -c("accountId")]
  flog.info("finish filter out history visit interaction, dim=(%s)",paste(dim(INTERACTIONS_REP),collapse=","))
  
  # get new rep list
  newRepList <- loadAnchorData.newRepList(INTERACTIONS_REP, repAccountAssignments)
  
  # get data requried for calendar adherence
  repCalendarAdherence <- NULL
  repAccountCalendarAdherence <- NULL
  callHistory <- NULL
  if (calendarAdherenceOn) {
    if (calculate_calendarAdherence) {
      flog.info("calendarAdherence=On & calculate_calendarAdherence=True, loading CallHistory for calculating calendarAdherence with startDate='%s',predictRundeate='%s", as.character(startDate), as.character(predictRundate))
      # load callHistory
      callHistory <- loadAnchorData.CallHistory(startDate, predictRundate)
      # join with rep & account table to get repId & accountId instead of repUID & accountUID
      accounts <- loadAnchorData.accounts()
      callHistory <- merge(callHistory, validReps, by="repUID")
      callHistory <- merge(callHistory, accounts, by="accountUID")
      callHistory[,c("repUID", "accountUID"):=NULL]
    } else {
      repCalendarAdherence <- loadAnchorData.repCalendarAdherence()
      repAccountCalendarAdherence <- loadAnchorData.repAccountCalendarAdherence()
    }
  }
  
  flog.info('Return form loadAnchorData')
  return(list(INTERACTIONS_REP=INTERACTIONS_REP, future=future, repAccountAssignments=repAccountAssignments, repTeamRep=repTeamRep, INTERACTIONS_ACCOUNT=INTERACTIONS_ACCOUNT, accountDateLocationScores=accountDateLocationScores, newRepList=newRepList, repCalendarAdherence=repCalendarAdherence, repAccountCalendarAdherence=repAccountCalendarAdherence, callHistory=callHistory))
  
}