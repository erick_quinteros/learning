##########################################################
#
#
# aktana-learning Anchor estimates Aktana Learning Engines.
#
# description: process accountDateLocation result from calculateAccountDateLocation 
#
#
# created by : jacob.adicoff@aktana.com
# updated by : jacob.adicoff@aktana.com
#
# created on : 2018-11-26
# updated on : 2018-11-26
#
# Copyright AKTANA (c) 2015.
#
#
####################################################################################################################
library(futile.logger)
library(data.table)

processAccountDateLocationResult <- function(accountDateLocation, predictAheadDayList, predictRundate) {
  
  flog.info("Start processAccountDateLocationResult with accountDateLocation result dim=(%s)",paste(dim(accountDateLocation),collapse=","))

  predictDates <- data.table(date=predictAheadDayList)
  predictDates[,dayOfWeek:=weekdays(as.Date(date))]
  
  # will get data expansion if dayList > 5 days. Reason for allow.cartesian=TRUE
  accountDateLocation <- merge(accountDateLocation, predictDates, by="dayOfWeek", allow.cartesian=TRUE)
  # drop col not needed
  accountDateLocation$dayOfWeek <- NULL
  
  # filter out current rundate if any & and add predictRundate
  accountDateLocation <- accountDateLocation[accountDateLocation$date!=predictRundate,]
  accountDateLocation$runDate <- predictRundate
  
  flog.info(sprintf('Return from processAccountDateLocationResult, dim(processed_accountDateLocation)=(%s)', paste(dim(accountDateLocation),collapse=",")))	
  return(accountDateLocation)
}