
loadAnchorData <- function(u,p,h,schema,port,startdate)
    {
        library(RMySQL)
        library(data.table)

        flog.info("loadAnchorData %s %s %s %s",u,p,h,schema)

        drv <- dbDriver("MySQL")

        tryCatch(con <- dbConnect(drv,user=u,password=p,host=h, port=port), 
                 error = function(e) {
                   flog.error('Error in connecting to db, user: %s', dbuser, name='error')
                   quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
                 })

        dbGetQuery(con,"SET NAMES utf8")

        Q <- paste("SELECT  v.repId, f.latitude, f.longitude, v.startDateLocal FROM ",
             paste(schema,".Interaction",sep="")," as v JOIN ",
             paste(schema,".Facility",sep="")," f using (facilityId) JOIN ",
             paste(schema,".Rep",sep=""), " r using (repId) JOIN ",
             paste(schema,".InteractionType",sep="")," i using (interactionTypeId) WHERE i.interactionTypeName LIKE \"%VISIT\" AND v.isCompleted=1 AND v.isDeleted=0 AND r.seConfigId>0;")

        interactions <- data.table(dbGetQuery(con,Q))
        interactions$date <- as.Date(interactions$startDateLocal)
        interactions$startDateLocal <- NULL
        interactions <- interactions[date>startdate]

        flog.info("interactions dim= %s %s",dim(interactions)[1],dim(interactions)[2])

        dbDisconnect(con)
        return(interactions)
    }

