#!/bin/bash

# fucntion to read config
function readJson {
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi;

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    #echo "Error: Cannot find \"${2}\" in ${1}, set to empty" >&2;
    echo '';
  else
    echo $VALUE ;
  fi;
}

# Set the learning home location if not set already (use absolute path)
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
fi
HOME_DIR_PARAM='homedir=\"'"$LEARNING_HOME"'\"'

# read parameters from command line
while getopts m:f:h:s:u:w:p: name
do
  case $name in
    m)MODULE=$OPTARG;;
    f)FUNC=$OPTARG;;
    h)DB_HOST=$OPTARG;;
    s)DB_NAME=$OPTARG;;
    u)DB_USER=$OPTARG;;
    w)DB_PASSWORD=$OPTARG;;
    p)DB_PORT=$OPTARG;;
    *)echo "Invalid arg";;
  esac
done

# get parameters from config file or set as default if not passed from command line

# If module is not defined.
if [ -z "$MODULE" ] ;
then
   echo "Error: MODULE (-m) is not defined correctly."
   exit 1
else
   TEST_DIR_PARAM='testdir=\"'"$MODULE/tests"'\"'
fi

# if func is not define, set to all as default
if [ -z "$FUNC" ] ;
then
   FUNC="all"
fi
FUNC_PARAM='func=\"'"$FUNC"'\"'

# if no dbhost, dbusername, dbuser, dbname, dbport get from config
if [ -z "$DB_HOST" ] ; then
   DB_HOST=`readJson $LEARNING_HOME/common/unitTest/config.json dbhost`
fi
DB_HOST_PARAM='dbhost=\"'"$DB_HOST"'\"'

if [ -z "$DB_USER" ] ; then
   DB_USER=`readJson $LEARNING_HOME/common/unitTest/config.json dbuser`
fi
DB_USER_PARAM='dbuser=\"'"$DB_USER"'\"'

if [ -z "$DB_PASSWORD" ] ; then
   DB_PASSWORD=`readJson $LEARNING_HOME/common/unitTest/config.json dbpassword`
fi
DB_PASSWORD_PARAM='dbpassword=\"'"$DB_PASSWORD"'\"'

if [ -z "$DB_NAME" ] ; then
   DB_NAME=`readJson $LEARNING_HOME/common/unitTest/config.json dbname`
fi
DB_NAME_PARAM='dbname=\"'"$DB_NAME"'\"'

if [ -z "$DB_PORT" ] ; then
   DB_PORT=`readJson $LEARNING_HOME/common/unitTest/config.json dbport`
fi
if [ "$DB_PORT" == "" ]
then
   DB_PORT_PARAM=''
else
   DB_PORT_PARAM='port="'"$DB_PORT"'"'
fi

CUSTOMER_PARAM='customer=\"'"unittest"'\"'

# # get additional parameters from test folder config file
# BUILD_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json buildUID`
# if [ "$BUILD_UID" == "" ]
# then
#    BUILD_UID_PARAM=''
# else
#    BUILD_UID_PARAM='BUILD_UID=\"'"$BUILD_UID"'\"'
# fi
#
# CONFIG_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json configUID`
# if [ "$CONFIG_UID" == "" ]
# then
#    CONFIG_UID_PARAM=''
# else
#    CONFIG_UID_PARAM='CONFIG_UID=\"'"$CONFIG_UID"'\"'
# fi
#
# RUN_UID=`readJson $LEARNING_HOME/$MODULE/tests/config.json runUID`
# if [ "$RUN_UID" == "" ]
# then
#    RUN_UID_PARAM=''
# else
#    RUN_UID_PARAM='RUN_UID=\"'"$RUN_UID"'\"'
# fi
#
# DEPLOY_ON_SUCCESS=`readJson $LEARNING_HOME/$MODULE/tests/config.json deployOnSuccess`
# if [ "$DEPLOY_ON_SUCCESS" == "" ]
# then
#    DEPLOY_ON_SUCCESS_PARAM=''
# else
#    DEPLOY_ON_SUCCESS_PARAM='deployOnSuccess=\"'"$DEPLOY_ON_SUCCESS"'\"'
# fi

RCMD="Rscript"
RSCRIPT="$LEARNING_HOME/common/unitTest/unitTestDriver.R"
RARGS="$HOME_DIR_PARAM $TEST_DIR_PARAM $FUNC_PARAM \
        $DB_USER_PARAM $DB_PASSWORD_PARAM $DB_HOST_PARAM $DB_NAME_PARAM $DB_PORT_PARAM $CUSTOMER_PARAM"
        # \ $BUILD_UID_PARAM $CONFIG_UID_PARAM $RUN_UID_PARAM \
        # $DEPLOY_ON_SUCCESS_PARAM

RFULLCMD="$RCMD $RSCRIPT $RARGS"

# Verify if the module is in place.
if [ ! -f "$RSCRIPT" ] ; then
        echo "Error: Could not find $$RSCRIPT"
        exit 1
fi

echo "running unit test on $MODULE:func $FUNC"
echo $RFULLCMD
eval $RFULLCMD


rc=$?
echo $rc

case $rc in
  0) echo "successfully running unit test on $MODULE:func $FUNC";;
  *) echo "failed running unit test on $MODULE:func $FUNC";;
esac

exit $rc
