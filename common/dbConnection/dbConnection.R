##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: database connection common function
##
## created by : marc.cohen@aktana.com
## updated by : shirley.xu@aktana.com
##
## created on : 2015-11-03
## updated on : 2018-09-17
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################

library(RMySQL)

################################################
## function: get connection handle of DSE DB
################################################
getDBConnection <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  drv <- dbDriver("MySQL")
  tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname,port=port), 
           error = function(e) {
             flog.error('Error in connecting to db %s: %s', dbname, e, name='error')
             quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
           })
  setDBDataEncoding(con)
  return(con) 
}


################################################
## function: set DB using utf-8 encoding when fetching data 
################################################
setDBDataEncoding <- function(con)
{
  dbClearResult(dbSendQuery(con,"SET NAMES utf8;"))
}

#################################################
## function: get connection handle of learning DB
#################################################
getDBConnectionLearning <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  dbnameLearning <- sprintf("%s_learning", dbname)
  con_l <- getDBConnection(dbuser, dbpassword, dbhost, dbnameLearning, port)
  return(con_l) 
}

#################################################
## function: get connection handle of _stage DB
#################################################
getDBConnectionStage <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  dbname_stage <- sprintf("%s_stage", dbname)
  con_stage <- getDBConnection(dbuser, dbpassword, dbhost, dbname_stage, port)
  return(con_stage) 
}

#################################################
## function: get connection handle of _cs DB
#################################################
getDBConnectionCS <- function(dbuser, dbpassword, dbhost, dbname, port)
{
  dbname_cs <- paste0(dbname,"_cs")
  if(substr(dbname,1,8)=="pfizerus") dbname_cs <- "pfizerprod_cs"  # this is exception for pfizerus (from messageClustering)
  con_cs <- getDBConnection(dbuser, dbpassword, dbhost, dbname_cs, port)
  return(con_cs) 
}