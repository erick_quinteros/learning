##########################################################
#
#
# aktana- update API config for different envs
#
# description: update API config for different envs
#
# created by : learning-dev@aktana.com
#
# created on : 2019-06-05
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################
library(futile.logger)
library(jsonlite)

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
  print("No arguments supplied.")
  if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}

# API config file path
localAPIConfigFilePath <- sprintf("%s/common/sparkUtils/APIConfig.json", homedir)

# # if save on s3, copy from s3 first
# S3APIConfigFilePath <- sprintf("s3://aktana-bdpus-kafka/learningdata/%s/APIConfig.json", customer)
# shellCode <- sprintf("aws s3 cp %s %s", S3APIConfigFilePath, localAPIConfigFilePath)
# system(shellCode)

# read config
newAPIConfig <- fromJSON(localAPIConfigFilePath)[[customer]][[envname]]

# overwrite API config in common/APICall/envs/env.json
if (!is.null(newAPIConfig)) {
  targetAPIConfigFilePath <- sprintf("%s/common/APICall/envs/env.json", homedir)
  flog.info("change learning API to: %s", newAPIConfig$learningApi)
  write_json(newAPIConfig, targetAPIConfigFilePath)
}

# write s3 bucket path to file for later use
tryCatch({
  # source API call related scripts
  source(sprintf("%s/common/APICall/global.R",homedir))
  # initialize API call params
  init(TRUE, paste(homedir,"/common/APICall",sep=""))
  # Authenticate and get a token
  Authenticate(glblLearningAPIurl)
  # call S3/pathPrefix API
  req <- getAPIJson(glblLearningAPIurl, 'S3/pathPrefix')
  s3BucketPath <- httr::content(req, as = "text")
  flog.info("s3BucketPath for %s/%s is: %s", customer, envname, s3BucketPath)
  # write to json path
  targetS3ConfigFilePath <- sprintf("%s/common/sparkUtils/s3Config.json", homedir)
  write_json(list(s3BucketPath=s3BucketPath, s3BuildDirPath=paste(s3BucketPath,'builds',sep='/')), targetS3ConfigFilePath)
},
error = function(e) {flog.error("Creating s3 path config failed with API Call: %s", e)}
)