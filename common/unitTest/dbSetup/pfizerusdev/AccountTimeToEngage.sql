CREATE TABLE `AccountTimeToEngage` (
  `accountId` int(11) NOT NULL DEFAULT '0',
  `repActionTypeId` tinyint(4) NOT NULL DEFAULT '0',
  `method` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `predictionDate` date DEFAULT '2017-11-30',
  `probability` double DEFAULT NULL,
  `runDate` date DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tteAlgorithmName` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `fk_tteAlgorithm` (`tteAlgorithmName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
