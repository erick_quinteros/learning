CREATE TABLE `MessageSetMessage` (
  `messageSetId` int(11) NOT NULL,
  `messageId` int(11) NOT NULL,
  `sequenceOrder` int(11) NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`messageSetId`,`sequenceOrder`),
  KEY `messageSetMessage_messageSetId` (`messageSetId`),
  KEY `messageSetMessage_messageId` (`messageId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci