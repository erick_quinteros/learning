CREATE TABLE `TTEsegmentReport` (
  `learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segment` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `segmentType` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `channelUID` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `intervalOrDoW` int(11) NOT NULL DEFAULT '1',
  `probability` double DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `method` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A',
  `tteAlgorithmName` varchar(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
);