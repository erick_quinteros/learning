# read args passed
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
#    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
        print("Arguments supplied.")
        for(i in 1:length(args)){
          eval(parse(text=args[[i]]))
          print(args[[i]]);
    }
}

if (!exists('port')) {
  port <<- 0
}

# get _cs, _learning,_stage DB name and match with other mockdata file
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))

# run tests
library(testthat)
if (func=='all') {
  test_dir(sprintf('%s/%s/',homedir,testdir), reporter=MultiReporter$new(reporters=list(ProgressReporter$new(),FailReporter$new())))
} else {
  test_file(sprintf('%s/%s/test_%s.r',homedir,testdir,func), reporter=MultiReporter$new(reporters=list(ProgressReporter$new(),FailReporter$new())))
}

# runscript
# Rscript common/unitTest/unitTestDriver.R homedir="'"'/Users/jiajingxu/Documents/learning'"'" func="'"'all'"'" testdir="'"'/common/unitTest/tests'"'"