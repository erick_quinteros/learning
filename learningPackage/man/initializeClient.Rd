\name{initializeClient}
\alias{initializeClient}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{initializeClient}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
initializeClient(c)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{c}{
%%     ~~Describe \code{c} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (c) 
{
    CPORT <<- NULL
    CATALOGENTRY <<- ""
    options(error = cleanUp)
    runStamp <<- gsub(":| |-", "_", sprintf("\%s_\%s", CLIENT, 
        Sys.time()))
    runDir <<- sprintf("./\%s/Run_\%s", CLIENT, runStamp)
    if (!dir.create(runDir, recursive = T)) 
        quit()
    plotDir <<- sprintf("\%s/plot_output_\%s", runDir, runStamp)
    plotRef <<- sprintf("./plot_output_\%s", runStamp)
    if (!dir.create(plotDir, recursive = T)) 
        quit()
    catalogFile <<- sprintf("\%s/Catalog.txt", CLIENT)
    flog.appender(appender.console())
    flog.appender(appender.file(sprintf("\%s/log.txt", runDir)))
    flog.threshold(INFO)
    flog.info("BuildDate = \%s", as.character(functionLibrary[["buildDate"]]))
    flog.info("Run directory = \%s", runDir)
    sink(sprintf("\%s/print.txt", runDir))
    WORKBOOK <<- createWorkbook()
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
