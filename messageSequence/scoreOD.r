##########################################################
#
#
# aktana- messageSequence Scoring On-Demand.
#
# description: driver pgm
#
# created by : wendong.zhu@aktana.com
#
# created on : 2017-12-28
#
# Copyright AKTANA (c) 2017.
#
#
####################################################################################################################
library(RMySQL)
library(data.table)
library(openxlsx)
library(properties)
library(uuid)
library(futile.logger)

#################################################
## function: get connection handle of learning DB
#################################################
getDBConnectionLearning <- function()
{
  dbnameLearning <- sprintf("%s_learning", dbname)
  drv <- dbDriver("MySQL")

  if(exists("port")){
      tryCatch(con_l <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbnameLearning,port=port), 
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbnameLearning, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  } else {
      tryCatch(con_l <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbnameLearning), 
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbnameLearning, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  }   

  return(con_l) 
}

################################################
## function: get connection handle of DSE DB
################################################
getDBConnection <- function()
{
  drv <- dbDriver("MySQL")

  if(exists("port")){
      tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname,port=port), 
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbname, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  } else {
      tryCatch(con <- dbConnect(drv,user=dbuser,password=dbpassword,host=dbhost,dbname=dbname), 
               error = function(e) {
                 flog.error('Error in connecting to db: %s', dbname, name='error')
                 quit(save = "no", status = 64, runLast = FALSE) # user-defined error code 64 for failure of connecting db
               })
  }   

  return(con) 
}

###################################################################################
## function: get the needed scores from AccountMessageSequence table in Learning DB
###################################################################################
getMscores <- function(con_l, whiteList, msmIds)
{
  scores_ams <- data.table(dbGetQuery(con_l, "select accountUID, messageUID, probability from AccountMessageSequence;"))

  # find needed messages by accuntUID, messageUID
  scores_acc <- scores_ams[accountUID %in% whiteList] 
  scores_msg <- scores_acc[messageUID %in% msmIds]

  return(scores_msg);
}

############################################################################
## function: save the needed scores into AccountMessageSequence table DSE DB 
############################################################################
saveScores2DSE <- function(con, scoresM, MODEL, MESSAGEALGORITHMID)
{
  FIELDS <- list(accountId="INT", messageAlgorithmId="INT", messageId="INT", modelId="varchar(80)", probability="double", predict="tinyint", createdAt="datetime", updatedAt="datetime")

  # set default values
  scoresM$predict <- 1
  scoresM$modelId <- MODEL 
  scoresM$messageAlgorithmId <- MESSAGEALGORITHMID

  nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
  scores$createdAt <- nowTime
  scores$updatedAt <- nowTime

  # change column names for DSE table 
  setnames(scoresM, c("accountUID","messageUID"), c("accountId","messageId")) 
  
  # convert columns from char to int for DSE table
  scoresM[, c("accountId","messageId") := list(as.integer(accountId), as.integer(messageId))] 

  # delete old scores first
  dbGetQuery(con, sprintf("DELETE FROM AccountMessageSequence WHERE messageAlgorithmId=%s",MESSAGEALGORITHMID))
  
  # Now insert the new scores to AccountMessageSequence in DSE
  tryCatch(dbWriteTable(con, name="AccountMessageSequence", value=as.data.frame(scoresM), overwrite=FALSE, append=TRUE, row.names=FALSE, field.types=FIELDS),
            error = function(e) {
               flog.error('Error in writing back to table AccountMessageSequence!', name='error')
               quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
           }) 
}

#####################################################
## main program
#####################################################

# parameter defaults
configName <- "None"
MODEL <- "None"
batch <- TRUE
DRIVERMODULE <- "scoreOD.R"

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
    print("Arguments supplied.")
    for(i in 1:length(args)){
       eval(parse(text=args[[i]]))
       print(args[[i]]);
    }
}

MODEL <- "All_Active"

# this needs to run in the global environment
library(Learning)
for(f in ls(package:Learning)) eval(parse(text=sprintf("environment(%s) <- globalenv()",f)))

source(sprintf("%s/messageSequence/whiteList.r",homedir)) 

RUN_UID <- ""  # initialize RUN_UID so that next source command won't get error of undefined RUN_UID

CONFIGURATION <- NULL
modelsToScore <- data.table(externalId=MODEL,messageSetId=0,messageAlgorithmId=0)
TargetNames <- NULL

# establish db connection to learning DB and DSE DB
con_l <- getDBConnectionLearning()
con   <- getDBConnection()

# read algorithm table and prepare to loop through all active models
messageSetMessage <- data.table(dbGetQuery(con,"Select messageSetId, messageId from MessageSetMessage;"))
messagesets <- data.table(dbGetQuery(con,"Select messageSetId, messageAlgorithmId from MessageSet;"))
modelsToScore <- data.table(dbGetQuery(con,"Select messageAlgorithmId, externalId from MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1;"))
modelsToScore <- merge(messagesets,modelsToScore,by="messageAlgorithmId") # natural join with all=FALSE by default; could be empty if non-match

All_Active_Flag <- T

# if modelsToScore is empty, give warning and exit
if (nrow(modelsToScore) == 0) {
  flog.warn("No model to score!")
  quit(save = "no", status = 1, runLast = FALSE) # exit
}

RUN_UID <- UUIDgenerate()

uniqueMessageAlgorithm <- unique(modelsToScore$messageAlgorithmId)

# loop through messageAlgorithmID
for(MESSAGEALGORITHMID in uniqueMessageAlgorithm)
{
    MODEL <- unique(modelsToScore[messageAlgorithmId==MESSAGEALGORITHMID]$externalId)
    flog.info("Score messageAlgorithmId: %s",MESSAGEALGORITHMID)
    if(length(MODEL)!=1){ flog.warn("Non unique modelID with messageAlgorithmId. Using first one."); MODEL <- MODEL[1] }
    flog.info("Score modelId: %s",MODEL)
   
    # add all the message names for the current messageset to pass to scoreModel() 
    if(exists("messageSetMessage"))
    {
        tt <- modelsToScore[messageAlgorithmId==MESSAGEALGORITHMID]$messageSetId  # find messageSetIds
        msmIds <- messageSetMessage[messageSetId %in% tt]$messageId               # find messageSetMessage messageIds
    }
    else
    {
        flog.error('messageSetMessage table does not exist! Cannot do scoring job!', name='error')
        quit(save = "no", status = 1, runLast = FALSE)  # exit due to non-existing messageSetMessage
    }

    # get whitelistDB
    whiteListDB <- whiteList(con, con_l, RUN_UID, BUILD_UID, MODEL, dbname, homedir) 

    whiteList <- whiteListDB$accountId     # contains the accounts in the whitelist

    # Get the needed scores from AccountMessageSequence table in Learning DB
    scoresM <- getMscores(con_l, whiteList, msmIds)

    # Save the needed scores into AccountMessageSequence table DSE DB 
    saveScores2DSE(con, scoresM, MODEL, MESSAGEALGORITHMID)
}

# clean up
dbDisconnect(con_l)
dbDisconnect(con)


