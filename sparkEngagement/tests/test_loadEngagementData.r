context('testing loadEngagementData() func in spark REM module')
print(Sys.time())

# load library and source script
library(RMySQL)
library(properties)
library(futile.logger)
source(sprintf("%s/sparkEngagement/code/loadEngagementData.R",homedir))
# required to run tests
library(sparkLearning)
library(arrow)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list() # use module own data
requiredMockDataList_module <- list(pfizerusdev=c('Interaction','InteractionAccount','Account','Rep','RepAccountAssignment'),pfizerusdev_stage=c('Suggestion_Feedback_vod__c_arc','RecordType_vod__c_arc','Suggestion_vod__c_arc'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList,requiredMockDataList_module,'engagement')

# set up unit test build folder and related configs
# get buildUID
BUILD_UID <- readModuleConfig(homedir, 'engagement','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'engagement', BUILD_UID, files_to_copy='learning.properties')

# parameters needed to run func
propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,BUILD_UID)
config <- read.properties(propertiesFilePath)
startHorizon <- as.Date(config[["LE_RE_startHorizon"]])  # make the startHorizon a date
useForProbability <- config[["LE_RE_useForProbability"]] # parsing of useForProbability
# con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)

# spark setting
library(sparkLearning)
sc <- initializeSpark(homedir)
sparkDBconURL <- sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname, port)
sparkDBconURL_stage <- sparkGetDBConnectionStage(dbuser, dbpassword, dbhost, dbname, port)

# run script
loadedData <- loadEngagementData(sc, sparkDBconURL, sparkDBconURL_stage, startHorizon, useForProbability)
interactions_new <- loadedData[['interactions']]
suggestions_new <- loadedData[['suggestions']]
rm(loadedData)

# test case
test_that("test have correct length of data", {
  expect_equal(sdf_dim(interactions_new),c(218,5))
  expect_equal(sdf_dim(suggestions_new),c(25,9))
})

test_that("test result is the same as the one saved ", {
  load(sprintf('%s/engagement/tests/data/from_loadEngagementData.RData', homedir))
  interactions <- data[[1]]
  suggestions <- data[[2]]
  setkey(interactions, NULL)
  interactions_new <- convertSDFToDF(interactions_new)
  suggestions_new <- convertSDFToDF(suggestions_new)
  expect_equal(interactions_new[order(accountId,repId,date,repActionTypeId),],interactions[order(accountId,repId,date,repActionTypeId),])
  expect_equal(suggestions_new[order(accountId,repId,date,type),],suggestions[order(accountId,repId,date,type),])
})

# disconnect spark
closeSpark(sc)