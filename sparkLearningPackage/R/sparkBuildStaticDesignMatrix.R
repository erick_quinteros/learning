##########################################################
##
##
## buildDesignMatrix
##
## description: static and dynamic design matrix build for
##              MSO build and score
##
## created by : marc.cohen@aktana.com
## updated by : shirley.xu@aktana.com
##
## created on : 2015-11-03
## updated on : 2018-09-17
##
## Copyright AKTANA (c) 2018.
##
##
##########################################################

sparkBuildStaticDesignMatrix <- function(prods, accountProduct, emailTopicNames, logDropProcess=TRUE) {
  
  # initialize map for predictor variable name in design matrix to original predictor name with colnames in Account/AccountProduct table & [,(,etc (map LoTProxy_akt__2_07_2_49_ to original LoTProxy_akt_(2_07,2_49])
  predictorNamesAPColMap <- list()
  
  if(length(prods)==0) { 
    prods <- "accountId"
    # initialize APpredictors for logging the droppend predictors in AccountProduct processing
    APpredictors <- data.frame(namesGroup=character(),dropReason=character(), isNum=numeric(), stringsAsFactors=FALSE)
  } else { 
    # initialize APpredictors for logging the droppend predictors in AccountProduct processing
    APpredictors <- data.frame(namesGroup=prods,dropReason=as.character(NA), isNum=0, stringsAsFactors=FALSE)
    prods <- c("accountId",prods)
  }
  
  # generate features from the clusters of physicalMessageUIDs estimated from the NLP analysis
  flog.info("Processing emailTopics name opens and clicks")
  sM <- accountProduct %>% select(one_of(c("accountId",emailTopicNames))) %>% na.replace(0)
  # t <- sparkMeltCombineDirect(sM, id_vars="accountId", var_name="variable", value_name="value", filterZero=TRUE)
  t <- sparkMeltCombine(sM, id_vars="accountId", var_name="variable", value_name="value", filterZero=TRUE, newColName="col", keepVarValueCols=TRUE)
  sM <- t %>% sdf_pivot(accountId~col, fun.aggregate=list(value="sum")) %>%
    na.replace(0)
  
  flog.info("Analyzing account product data")
  # prepare the accountProduct table and build design
  AP <- accountProduct %>% select(one_of(c(prods)))
  # change NA in string type column to "NA" so it will be counted as distinct count
  colTypes <- sdf_schema(AP)
  colTypes <- sapply(colTypes,function(x){x$type})
  chrV <- names(colTypes)[colTypes=="StringType"]
  AP <- AP %>% mutate_at(.vars=chrV,funs(ifelse(is.na(.),'NA',.)))
  # pick out variable withs more than 1 unique value (also drop productName column here)
  AP <- sparkFilterOutColSameValue(AP)

  if(sdf_ncol(AP) == 1) {
      # AP contains only accountId. no need to proceed. Return sM.
      flog.info("AP contains only accountId; return from buildStaticDesignMatrix")
      return(list(AP=sM, APpredictors=APpredictors, predictorNamesAPColMap=predictorNamesAPColMap))
  }  

  AP <- sdf_persist(AP)
  if (logDropProcess) {
    colsInAP <- colnames(AP)                                         # log dropped predictors
    droppedPredictors <- prods[!prods %in% colsInAP]
    flog.info("%s of %s predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value", length(droppedPredictors), length(prods)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in%droppedPredictors, c("dropReason")] <- "<2 unique"
    }
  }
  
  colTypes <- sdf_schema(AP)                                   # find the classes of those variables
  colTypes <- sapply(colTypes,function(x){x$type})
  numV <- names(colTypes)[colTypes=="IntegerType"|colTypes=="DoubleType"]     # pick out the non-character (numeric) variables
  APpredictors[APpredictors$namesGroup %in% numV, c('isNum')] <- 1  # log which variable is numeric, so could log where to convert back "_" to for spark dataframe
  
  # chrV <- names(colTypes)[colTypes=="StringType"]         # pick out the character variables
  # for(i in chrV)                                          # for the numeric variables bucket them based on quantiles
  # {                                                             # only do this if there are more than 5 unique values
  #   eval(parse(text=sprintf("AP <- mutate(AP, %s=ifelse(is.na(%s),'NA',%s))",i,i,i)))   #commented because conversion of NA to "NA" is automatically done when copying data frames to Spark
  # }
  processedDataList <- list()
  for(i in numV)                                      # for the numeric variables bucket them based on quantiles
  {                                                             # only do this if there are more than 5 unique values
    if(i=="accountId")next                                    # skip if it's the accountId
    colData <- eval(parse(text=sprintf("pull(AP,%s)",i)))
    n <- length(unique(colData))
    if(n>2)                                                   # only do this if there are more than 5 unique values
    {                                                         # could add logic here to make the cut finer - ie more buckets than just the quartiles
      q <- quantile(colData,probs=seq(0,1,ifelse(n>5,.25,.5)),na.rm=T)                  #  calculate quantiles
      if(sum(q>0)<2){ eval(parse(text=sprintf("AP <- select(AP, -%s) ",i))) } else {
        processedColData <- gsub("\\.","_", as.character(cut(colData,unique(q),include.lowest=T)))
        processedColData[is.na(processedColData)] <- "missing"
        processedDataList[[i]] <- processedColData }
    } else { eval(parse(text=sprintf("if(sdf_dim(distinct(filter((select(AP,%s)),!is.na(%s))))[1]<2) { AP <- select(AP, -%s)} ",i,i,i))) }
  }

  if (length(processedDataList)>0) {
    processedData <- sdf_copy_to(spark_connection(AP),data.frame(processedDataList,stringsAsFactors=FALSE), "processedData", overwrite=FALSE, memory=FALSE, repartition=48)
    # rm(processedDataList)

    AP <- AP %>% select(-one_of(colnames(processedData))) %>% sdf_bind_cols(processedData)
  }
  AP <- sdf_persist(AP)

  if (logDropProcess) {
    droppedPredictors <- colsInAP[!colsInAP %in% colnames(AP)]       # log dropped predictors
    flog.info("%s of %s numeric predictors (%s all predictors) from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0", length(droppedPredictors), length(numV)-1, length(colsInAP)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    colsInAP <- colnames(AP)
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in%droppedPredictors, c("dropReason")] <- "<5 unique numeric or <2 quantile>0"
    }
  }
  
  AP <- sparkFilterOutColSameValue(AP)   # pick out variable withs more than 1 unique value
  
  if (logDropProcess) {
    droppedPredictors <- colsInAP[!colsInAP %in% colnames(AP)]       # log dropped predictors
    flog.info("%s of %s remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately", length(droppedPredictors), length(colsInAP)-1)
    flog.debug(paste(droppedPredictors, collapse=","))
    colsInAP <- colnames(AP)
    if (dim(APpredictors)[1]>0) { # prods configs >0
      APpredictors[APpredictors$namesGroup %in%droppedPredictors, c("dropReason")] <- "<2 unique"
    }
  }
  
  #    AP <- data.table(model.matrix(~-1+.,AP))                      # now that everything in the AccountProduct table is char and/or factor build the core design matrix without an intercept
  tmp <- AP %>% mutate_at(.vars=colnames(AP)[colnames(AP)!="accountId"],funs(as.character(.))) %>%               # convert AP columns types all to character for melt
    sparkMelt(id_vars="accountId") %>%
    mutate(value = ifelse(value=="","missing",value)) %>%         # this section replaces the model.matrix() to avoid the contasts issues
    mutate(value = ifelse(is.na(value), "NA", value)) %>%      # this make sure it map NA as "NA"
    # na.replace("missing") %>%
    # mutate(value = regexp_replace(value,"^NA$","missing")) %>%    # replace NA and blank with missing
    sparkCombineCols(c("variable","value"), sep="_", newColName="newvariable", keepOriCols=FALSE) %>% # construct new variable names
    mutate(newvalue = as.numeric(1))                                        # create value var
  AP <- tmp %>% 
    sdf_pivot(accountId~newvariable, fun.aggregate=list(newvalue="sum")) %>%
    na.replace(0)                                                # cast - basically a transpose
  # if (removeSpecialCharacterInAP) {
  #   names(AP) <- gsub("\n","",names(AP))                          # remove problematic character in colnames after dcast
  # }                                                            # comment out as this is always done when converting to spark dataframe
  
  # rm(tmp)
  if (logDropProcess) {
    flog.info("After doing dcast, num of predictors in AccountProduct/Account changed from %s to %s", length(colsInAP)-1, sdf_dim(AP)[2]-1)
    # rm(colsInAP)
  }
  
  # remove all special character in AP colnames (predictor variable name), and save the mapping
  originalColnames <- copy(colnames(AP))
  originalColnames <- originalColnames[originalColnames!="accountId"]
  newColnames <- md5(originalColnames)
  renameStr <- character()
  for (i in 1:length(originalColnames)) {
    newColname <- newColnames[i]
    oldColname <- originalColnames[i]
    if (newColname != oldColname) {
      predictorNamesAPColMap[[newColname]] <- oldColname
      renameStr <- c(renameStr, sprintf("`%s`=`%s`", newColname, oldColname, sep=','))
    }
  }
  if (length(renameStr)>0) {
    renameStr <- paste(renameStr, collapse=', ')
    eval(parse(text=sprintf("AP <- rename(AP, %s) ",renameStr)))
  }
  
  # 
  # now add the stable message stuff    
  AP<-left_join(AP,sM,by="accountId")                       # now merge the messageTopic counts into the AP matrix to complete the build of the design matrix
  flog.info("After adding predictors from EmailTopic Open Rates, final num of predictors in AccountProduct/Account added %s and becomes %s", sdf_dim(sM)[2]-1, sdf_dim(AP)[2]-1)
  # rm(sM)                                                        # done with sM so clean up memory
  
  flog.info("return from buildStaticDesignMatrix")
  return(list(AP=AP, APpredictors=APpredictors, predictorNamesAPColMap=predictorNamesAPColMap))
}
