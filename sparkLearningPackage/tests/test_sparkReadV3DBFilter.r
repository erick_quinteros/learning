context('testing readV3dbFilter from sparkLearningPackage')
print(Sys.time())

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','MessageSet','Event','EventType','Interaction','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account'))
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,requiredMockDataList)

# loading Learning package to call function for test
library(sparkLearning)
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/utilsFuncSpark.r',homedir))

# set required paramter
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)

# spark setting
library(sparkLearning)
sc <- initializeSpark(homedir)
sparkDBconURL <- sparkGetDBConnection(dbuser, dbpassword, dbhost, dbname, port)

# call readV3Filter for defined productUID
productUID <- readModuleConfig(homedir, 'common/unitTest','productUID')
dictraw <- sparkReadV3dbFilter(sc, sparkDBconURL, con, productUID)
names(dictraw) <- paste(names(dictraw),"_new",sep="")
for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])

# test case
test_that("test have correct length of data for defined productUID", {
  expect_equal(length(dictraw), 8)
  expect_equal(sdf_dim(accounts_new),c(310,46))
  expect_equal(sdf_dim(interactions_new),c(16592,12))
  expect_equal(dim(messages_new),c(446,7))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(messageSet_new),c(4,2))
  expect_equal(sdf_dim(accountProduct_new),c(309,170))
  expect_equal(sdf_dim(events_new),c(390,9))
  expect_equal(dim(products_new),c(1,2))
})
rm(dictraw)

test_that("result is the same as saved for defined productUID", {
  # load saved data for comparison
  load(sprintf("%s/common/unitTest/data/from_readV3db_accounts.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_interactions.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messages.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSetMessage.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSet.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_events.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_products.RData", homedir))
  # comapre with new results (data.table results)
  expect_equal(messages_new[order(messages$messageId),],messages[order(messages$messageId),])
  expect_equal(messageSet_new[order(messageSet_new$messageId),],messageSet[order(messageSet$messageId),])
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(data.table(products_new),products[productName=="CHANTIX"])
  # compare with new results (spark data frame results) - accounts
  accounts_new <- convertSDFToDF(accounts_new)
  accounts$createdAt <- as.POSIXct(accounts$createdAt)
  accounts$updatedAt <- as.POSIXct(accounts$updatedAt)
  expect_equal(accounts_new[order(accounts_new$accountId),-c("createdAt","updatedAt")],accounts[order(accounts$accountId),-c("createdAt","updatedAt")])   # time has different timestamp because jdbc timezone, setup, but since they are not used, it is fine to drop them for now
  # compare with new results (spark data frame results) - accountProduct
  accountProduct <- accountProduct[productName=="CHANTIX"]
  accountProduct$createdAt <- as.POSIXct(accountProduct$createdAt)
  accountProduct$updatedAt <- as.POSIXct(accountProduct$updatedAt)
  accountProduct_new <- convertSDFToDF(accountProduct_new)
  expect_equal(accountProduct_new[order(accountProduct_new$accountId),-c("createdAt","updatedAt")],accountProduct[order(accountProduct$accountId),-c("createdAt","updatedAt")])    # time has different timestamp because jdbc timezone, setup, but since they are not used, it is fine to drop them for now
  # compare with new results (spark data frame results) - interactions
  interactions$productInteractionTypeName <- as.character(interactions$productInteractionTypeName)
  interactions$productName <- as.character(interactions$productName)
  interactions_new <- convertSDFToDF(interactions_new)
  cols <- colnames(interactions_new)
  expect_equal(interactions_new[order(interactions_new$interactionId, interactions_new$accountId),],interactions[order(interactions$interactionId, interactions$accountId),][interactions$productName=="CHANTIX",..cols])
  # compare with new results (spark data frame results) - events
  events$eventDate <- as.Date(events$eventDate)
  events$eventDateTimeUTC <- as.POSIXct(events$eventDateTimeUTC)
  events_new <- convertSDFToDF(events_new)
  expect_equal(events_new[order(events_new$repId, events_new$accountId, events_new$eventDate, events_new$eventTypeName, events_new$eventLabel),-c("eventDateTimeUTC")],events[order(events$repId, events$accountId, events$eventDate, events$eventTypeName, events$eventLabel),-c("eventDateTimeUTC")][productName=="CHANTIX"])
  
})

# call readV3dbFilter again for productUID=All and prod=NULL
productUID <- "All"
dictraw <- sparkReadV3dbFilter(sc, sparkDBconURL, con,productUID)
names(dictraw) <- paste(names(dictraw),"_new",sep="")
for(i in 1:length(dictraw))assign(names(dictraw)[i],dictraw[[i]])

# test case
test_that("test have correct length of data for productId=All", {
  expect_equal(length(dictraw), 8)
  expect_equal(sdf_dim(accounts_new),c(310,46))
  expect_equal(sdf_dim(interactions_new),c(47763,12))
  expect_equal(dim(messages_new),c(446,7))
  expect_equal(dim(messageSetMessage_new),c(10,2))
  expect_equal(dim(messageSet_new),c(4,2))
  expect_equal(sdf_dim(accountProduct_new),c(560,2))
  expect_equal(sdf_dim(events_new),c(733,9))
  expect_equal(dim(products_new),c(4,2))
})
rm(dictraw)

test_that("result is the same as saved for productId=All", {
  # load saved data for comparison
  load(sprintf("%s/common/unitTest/data/from_readV3db_accounts.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_interactions.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messages.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSetMessage.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_messageSet.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_events.RData", homedir))
  load(sprintf("%s/common/unitTest/data/from_readV3db_products.RData", homedir))
  # comapre with new results (data.table results)
  expect_equal(messages_new[order(messages$messageId),],messages[order(messages$messageId),])
  expect_equal(messageSet_new[order(messageSet_new$messageId),],messageSet[order(messageSet$messageId),])
  expect_equal(messageSetMessage_new,messageSetMessage)
  expect_equal(data.table(products_new),products)
  # compare with new results (spark data frame results) - accounts
  accounts_new <- convertSDFToDF(accounts_new)
  accounts$createdAt <- as.POSIXct(accounts$createdAt)
  accounts$updatedAt <- as.POSIXct(accounts$updatedAt)
  expect_equal(accounts_new[order(accounts_new$accountId),-c("createdAt","updatedAt")],accounts[order(accounts$accountId),-c("createdAt","updatedAt")])   # time has different timestamp because jdbc timezone, setup, but since they are not used, it is fine to drop them for now
  # compare with new results (spark data frame results) - accountProduct
  accountProduct$createdAt <- as.POSIXct(accountProduct$createdAt)
  accountProduct$updatedAt <- as.POSIXct(accountProduct$updatedAt)
  accountProduct_new <- convertSDFToDF(accountProduct_new)
  expect_equal(accountProduct_new[order(accountProduct_new$accountId, accountProduct_new$productName),],accountProduct[order(accountProduct$accountId, accountProduct$productName),c("accountId","productName")])    # time has different timestamp because jdbc timezone, setup, but since they are not used, it is fine to drop them for now
  # compare with new results (spark data frame results) - interactions
  interactions$productInteractionTypeName <- as.character(interactions$productInteractionTypeName)
  interactions$productName <- as.character(interactions$productName)
  interactions_new <- convertSDFToDF(interactions_new)
  cols <- colnames(interactions_new)
  expect_equal(interactions_new[order(interactions_new$interactionId, interactions_new$accountId, interactions_new$productName),],interactions[order(interactions$interactionId, interactions$accountId, interactions$productName),][,..cols])
  # compare with new results (spark data frame results) - events
  events$eventDate <- as.Date(events$eventDate)
  events$eventDateTimeUTC <- as.POSIXct(events$eventDateTimeUTC)
  events_new <- convertSDFToDF(events_new)
  expect_equal(events_new[order(events_new$repId, events_new$accountId, events_new$productName, events_new$eventDate, events_new$eventTypeName, events_new$eventLabel, events_new$physicalMessageUID),-c("eventDateTimeUTC")],events[order(events$repId, events$accountId, events$productName, events$eventDate, events$eventTypeName, events$eventLabel, events$physicalMessageUID),-c("eventDateTimeUTC")])
})

# call readV3dbFilter again for productUID=All and defined prods
load(sprintf("%s/common/unitTest/data/from_readV3db_accountProduct.RData", homedir))
productUID <- "All"
prods <- colnames(accountProduct)[!colnames(accountProduct) %in% c("accountId","productName")]
dictraw <- sparkReadV3dbFilter(sc, sparkDBconURL, con,productUID, prods=prods)
accountProduct_new <- dictraw[['accountProduct']]
test_that("check accountProduct for productId=All with defined prods", {
  expect_equal(sdf_dim(accountProduct_new),c(560,170))
  # compare with new results (spark data frame results) - accountProduct
  accountProduct$createdAt <- as.POSIXct(accountProduct$createdAt)
  accountProduct$updatedAt <- as.POSIXct(accountProduct$updatedAt)
  accountProduct_new <- convertSDFToDF(accountProduct_new)
  expect_equal(accountProduct_new[order(accountProduct_new$accountId, accountProduct_new$productName),-c("createdAt","updatedAt")],accountProduct[order(accountProduct$accountId, accountProduct$productName),-c("createdAt","updatedAt")])    # time has different timestamp because jdbc timezone, setup, but since they are not used, it is fine to drop them for now
  # compare with new results (spark data frame results) - interactions
})

# disconnect DB
dbDisconnect(con)

# disconnect spark
closeSpark(sc)
