
##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: save messageSequence results
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016-2018.
#
#
####################################################################################################################

saveTimingScoreResult <- function(sc, con, con_l, scores, BUILD_UID, RUN_UID, productUID, isNightly, runSettings, tteParams)
{
    library(data.table)
    library(futile.logger)
    library(ggplot2)
    library(scales)
    library(sparklyr)
    library(dplyr)
  
    flog.info("entered saveTimingScoreResult function")    

    jdbc.url <- sprintf("jdbc:mysql://%s:%s/%s?serverTimezone=UTC", dbhost, port, dbname)
    jdbc.url.learning <- sprintf("jdbc:mysql://%s:%s/%s_learning?serverTimezone=UTC", dbhost, port, dbname)
    
    plotDir <- runSettings[["plotDir"]]

    segs <- tteParams[["segs"]]
    channelUID <- tteParams[["channelUID"]]
    channels <- tteParams[["channels"]]
    runStamp <- tteParams[["runStamp"]]
    numPartitions <- tteParams[["NUM_PARTITIONS_W"]]
    
    #sqlStr <- "SELECT repActionTypeId, repActionTypeName FROM RepActionType"
    #repAction <- data.table(dbGetQuery(con, sqlStr))

    repAction <- spark_read_jdbc(sc, "RepActionType", options = list(
                     url = jdbc.url, user = dbuser, password = dbpassword,
                     driver='com.mysql.jdbc.Driver',
                     dbtable = "(SELECT repActionTypeId, repActionTypeName FROM RepActionType) as my_query",
                     numPartitions = as.character(numPartitions)))
    
    #setnames(scores, "date", "predictionDate") 
    #setnames(scores, "predict", "probability")
    scores <- dplyr::rename(scores, predictionDate = date, probability = predict)

    nowDate <- Sys.Date()
    nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

    if (F) {
        scoresSeg <- data.table()

        acctSeg <- accounts[, c("accountId", segs), with=F]
        scores.seg <- merge(scores, acctSeg, by="accountId")

        for (seg in segs) {
            scoresTmp <- scores.seg[, .(probability=sum(probability)/.N), by=c(seg, "event", "method", "predictionDate", "learningRunUID")]
            scoresTmp$segment <- seg 
            setnames(scoresTmp, seg, "segmentType")
            scoresSeg <- rbind(scoresTmp, scoresSeg, fill=T)
        }
            
        scoresSeg$createdAt <- nowTime
        scoresSeg$updatedAt <- nowTime 

        setnames(scoresSeg, "event", "channelUID")

        #dbGetQuery(con_l, "TRUNCATE TABLE SegmentTimeToEngage")
            
        FIELDS.seg <- list(segment="varchar(40)", segmentType="varchar(80)", channelUID="varchar(20)", method="varchar(40)", predictionDate="date", learningRunUID="varchar(80)", probability="double", createdAt="datetime", updatedAt="datetime")

        tryCatch(dbWriteTable(con_l, name="SegmentTimeToEngage", value=as.data.frame(scoresSeg), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS.seg),
            error = function(e) {
                 flog.error('Error in writing back to table SegmentTimeToEngage!', name='error')
                 dbDisconnect(con_l)
                 quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
            }
        )
    }
    
    if (isNightly) {
        # convert event type to id. 
        # Todo: id based on a list
        #scores[event == 'S',  event := '12']
        #scores[event == 'V', event := '3']
        #scores[event == 'W', event := '13']
        #scores[event == 'VS', event := '5']

        scores <- scores %>% dplyr::mutate(event = ifelse(event == 'S', "12", event))
        scores <- scores %>% dplyr::mutate(event = ifelse(event == 'V', "3",  event))
        scores <- scores %>% dplyr::mutate(event = ifelse(event == 'W', "13", event))
        scores <- scores %>% dplyr::mutate(event = ifelse(event == 'VS', "5", event))
          
        #scores[, event := as.integer(event)]
        scores <- scores %>% dplyr::mutate(event = as.integer(event))
        
        #setnames(scores, "event", "repActionTypeId")  # rename event to repActionTypeId
        scores <- dplyr::rename(scores, repActionTypeId = event)
        
        scores <- scores %>% dplyr::mutate(runDate = as.Date(nowDate), tteAlgorithmName = tteParams[["tteAlgorithmName"]])
        #scores$tteAlgorithmName <- tteParams[["tteAlgorithmName"]]       
        
        if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
            if (!"VISIT" %in% channels) {  
                # listen to SEND channel only, so we only output best time to send email after SEND
                # for other channels, always output best time to send email afer VISIT or EVENT
                flog.info("listen to SEND channel in nightly run.")
                #scores <- scores[repActionTypeId == 12] 
                scores <- scores %>% filter(repActionTypeId == 12)
            }
        }
        
        # delete old data first.
        startTimer <- Sys.time()
        dbGetQuery(con, sprintf("DELETE FROM AccountTimeToEngage WHERE tteAlgorithmName = '%s'", tteParams[["tteAlgorithmName"]]))
        
        flog.info("Delete old scores of AccountTimeToEngage: Time = %s", Sys.time()-startTimer)
        flog.info("Number of rows in scores = %s", sdf_dim(scores)[1])
        
        FIELDS <- list(accountId="int", repActionTypeId="tinyint", method="varchar(40)", tteAlgorithmName="varchar(80)", predictionDate="date", learningRunUID="varchar(80)", probability="double", runDate="date", createdAt="datetime", updatedAt="datetime")

        # append new scores/probs
        startTimer <- Sys.time()
        #tryCatch(dbWriteTable(con, name="AccountTimeToEngage", value=as.data.frame(scores), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
        #     error = function(e) {
        #       flog.error('Error in writing back to DSE table AccountTimeToEngage!', name='error')
        #       dbDisconnect(con)
        #       quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
        #     }
        #)
        scores <- sdf_repartition(scores, partitions = numPartitions)
        
        spark_write_jdbc(scores, name = "AccountTimeToEngage",
                         options = list(url = jdbc.url, driver='com.mysql.jdbc.Driver', user = dbuser, password = dbpassword), 
                         mode = "append")
        
        flog.info("Append new scores to DSE AccountTimeToEngage: Time = %s", Sys.time()-startTimer)            

        ## Plotting
        flog.info("Now plotting predictions for sample accounts ...")
        scores <- data.table(collect(scores))
        # sample accountIds and plot
        plotLoc <- sprintf("%s/Predictions_samples_SEND_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)

        for (id in sample(scores$accountId, 300)) {   
            print(ggplot(scores[accountId==id & repActionTypeId==12], aes(x=predictionDate, y=probability, color=probability))+geom_point()
            + labs(caption = sprintf("(accountId: %s)", id)) + scale_color_gradient(low="blue", high="red"))
        }
        dev.off()

        if(nrow(scores[repActionTypeId==3]) > 0) { # VISIT  
            plotLoc <- sprintf("%s/Predictions_samples_VISIT_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
            pdf(plotLoc)
    
            for (id in sample(scores$accountId, 300)) {  
                print(ggplot(scores[accountId==id & repActionTypeId==3], aes(x=predictionDate, y=probability, color=probability))+geom_point()
                + labs(caption = sprintf("(accountId: %s)", id)) + scale_color_gradient(low="blue", high="red"))
            }
            dev.off()
        }
        
        plotLoc <- sprintf("%s/Predictions_summary_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        print(ggplot(scores[repActionTypeId==12], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) + 
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (SEND)"))
        print(ggplot(scores[repActionTypeId==3], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) +
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (VISIT)"))
        dev.off()

    } else { # manual job and score into Learning DB
      
        FIELDS <- list(accountUID="varchar(80)", channelUID="varchar(80)", method="varchar(40)", predictionDate="date", learningRunUID="varchar(80)", probability="double", createdAt="datetime", updatedAt="datetime")
        
        #accountIdMap <- data.table(dbGetQuery(con,"SELECT accountId, externalId FROM Account WHERE isDeleted=0;"))
        accountIdMap <- spark_read_jdbc(sc, "Account", options = list(
                            url = jdbc.url, user = dbuser, password = dbpassword,
                            driver='com.mysql.jdbc.Driver',
                            dbtable = "(SELECT accountId, externalId FROM Account WHERE isDeleted=0) as my_query",
                            numPartitions = as.character(numPartitions)))
        
        # get accountUID from accountId
        scores <- scores %>% inner_join(accountIdMap, by="accountId")
        
        #setnames(scores, "externalId", "accountUID")
        #scores$accountId  <- NULL
        scores <- dplyr::rename(scores, accountUID = externalId) %>% select(-accountId)
        
        #scores[event == "S", channelUID := "SEND"]
        #scores[event == "V", channelUID := "VISIT"]
        #scores$event <- NULL
        scores <- scores %>% dplyr::mutate(channelUID = ifelse(event == "S", "SEND", event))
        scores <- scores %>% dplyr::mutate(channelUID = ifelse(event == "V", "VISIT", channelUID)) %>%
                             select(-event)
        
        if (channelUID == "SEND_CHANNEL") {  # optimize SEND channel
            if (!"VISIT" %in% channels) {
                # listen to SEND channel only, so we only output best time to send email after SEND
                # for other channels, always output best time to send email afer VISIT or EVENT
                flog.info("listen to SEND channel in manual run.")
                #scores <- scores[channelUID == "SEND"] 
                scores <- scores %>% filter(channelUID == "SEND")
            }
        }

        nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
        #scores <- scores %>% dplyr::mutate(createdAt = nowTime, updatedAt = nowTime)
        startTimer <- Sys.time()
        scores <- sdf_repartition(scores, partitions = numPartitions)

        startTimer <- Sys.time()
        dbGetQuery(con_l, "TRUNCATE TABLE AccountTimeToEngage")
        
        flog.info("Delete old scores of AccountTimeToEngage in LearningDB: Time = %s", Sys.time()-startTimer)
        flog.info("Number of rows in scores = %s", sdf_dim(scores)[1])
        
        # Now write the new scores to database 
        #tryCatch(dbWriteTable(con_l, name="AccountTimeToEngage", value=as.data.frame(scores), overwrite=F, append=T, row.names=FALSE, field.types=FIELDS),
        #     error = function(e) {
        #       flog.error('Error in writing back to table AccountTimeToEngage in learning DB!', name='error')
        #       dbDisconnect(con_l)
        #       quit(save = "no", status = 67, runLast = FALSE) # user-defined error code 67 for failure of writing back to database table
        #     }
        #)  
        spark_write_jdbc(scores, name = "AccountTimeToEngage",
                         options = list(url = jdbc.url.learning, driver='com.mysql.jdbc.Driver', user = dbuser, password = dbpassword), 
                         mode = "append")
        
        flog.info("Write scores to LearningDB AccountTimeToEngage Time = %s", Sys.time()-startTimer)  

        ## Plotting
        # sample accountUIDs and plot
        scores <- data.table(collect(scores))
        
        plotLoc <- sprintf("%s/Predictions_samples_SEND_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        for (id in sample(scores$accountUID, 300)) {
            print(ggplot(scores[accountUID==id & channelUID=="SEND"], aes(x=predictionDate, y=probability, color=probability))+geom_point()
            + labs(caption = sprintf("(accountUID: %s)", id)) + scale_color_gradient(low="blue", high="red"))
        }
        dev.off()

        if (nrow(scores[channelUID=="VISIT"]) > 0) {
            plotLoc <- sprintf("%s/Predictions_samples_VISIT_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
            pdf(plotLoc)
            for (id in sample(scores$accountUID, 300)) {
                print(ggplot(scores[accountUID==id & channelUID=="VISIT"], aes(x=predictionDate, y=probability, color=probability))+geom_point()
                + labs(caption = sprintf("(accountUID: %s)", id)) + scale_color_gradient(low="blue", high="red"))
            }
            dev.off()
        }
        
        plotLoc <- sprintf("%s/Predictions_summary_%s.pdf",plotDir,runStamp)  # plot the distribution for a sanity check if needed
        pdf(plotLoc)
        print(ggplot(scores[channelUID=="SEND"], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) + 
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (SEND)"))
        print(ggplot(scores[channelUID=="VISIT"], aes(x=probability)) + geom_histogram(aes(y = (..count..)/sum(..count..)), fill="blue", color="green" ) +
            scale_y_continuous(labels = scales::percent) +  ylab("Percentage of Total") + xlab("Probability (VISIT)"))
        dev.off()

    }

}
